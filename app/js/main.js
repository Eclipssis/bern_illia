jQuery(function($){

  $('input[placeholder], textarea[placeholder]').placeholder();

  // add-open-class
  $('.menu-icon').click(function(){
   if($(this).parent().is('.menu-open')){
     $(this).parent().removeClass('menu-open');
     $('body').removeClass('menu-open-wrapper-page');
   }else{
     $(this).parent().addClass('menu-open');
     $('body').addClass('menu-open-wrapper-page');
   }
  });

  $(".terms-nav").on("click","a", function (event) {
          //отменяем стандартную обработку нажатия по ссылке
          event.preventDefault();

          //забираем идентификатор бока с атрибута href

          var id  = $(this).attr('href'),

          //узнаем высоту от начала страницы до блока на который ссылается якорь

              top = $(id).offset().top;

          //анимируем переход на расстояние - top за 1500 мс

          $('body,html').animate({scrollTop: top}, 800);

      });



  $(".panel-default").hover(function() {
      $(this).find('.panel-collapse').collapse('show');
    }, function() {
      $(this).find('.panel-collapse').collapse('hide');
    }
  );

  var csObj = new Object();
    csObj.scrollButtons = {
        enable:true,
        scrollType:"pixels",
        scrollAmount:200
    };

    $(".scroll-bar").mCustomScrollbar(csObj);




   // var cash = $("#cash-value").html().slice(1)

  $( "#sliderPrice" ).slider({
    range: "min",
    value: 0.003,
    min: 0,
    max: 0.1,
    step: 0.001,
    slide: function( event, ui ) {
      $( "#amount" ).val( "$" + ui.value );
      // $("#cash-value").html("$" + (ui.value + parseInt(cash)) );
    }
  });
  $( "#amount" ).val( "$" + $( "#sliderPrice" ).slider( "value" ) );

  $( "#sliderPrice-2" ).slider({
    range: "min",
    value: 0.005,
    min: 0,
    max: 0.1,
    step: 0.001,
    slide: function( event, ui ) {
      $( "#amount-2" ).val( "$" + ui.value );
      // $("#cash-value").html("$" + (ui.value + parseInt(cash)) );
    }
  });
  $( "#amount-2" ).val( "$" + $( "#sliderPrice-2" ).slider( "value" ) );

  $( "#sliderPrice-3" ).slider({
    range: "min",
    value: 0.007,
    min: 0,
    max: 0.1,
    step: 0.001,
    slide: function( event, ui ) {
      $( "#amount-3" ).val( "$" + ui.value );
      // $("#cash-value").html("$" + (ui.value + parseInt(cash)) );
    }
  });
  $( "#amount-3" ).val( "$" + $( "#sliderPrice-3" ).slider( "value" ) );

  $( "#sliderPrice-4" ).slider({
    range: "min",
    value: 0.009,
    min: 0,
    max: 0.1,
    step: 0.001,
    slide: function( event, ui ) {
      $( "#amount-4" ).val( "$" + ui.value );
      // $("#cash-value").html("$" + (ui.value + parseInt(cash)) );
    }
  });
  $( "#amount-4" ).val( "$" + $( "#sliderPrice-4" ).slider( "value" ) );





});//end ready


//Plugin placeholder
(function(b,f,i){function l(){b(this).find(c).each(j)}function m(a){for(var a=a.attributes,b={},c=/^jQuery\d+/,e=0;e<a.length;e++)if(a[e].specified&&!c.test(a[e].name))b[a[e].name]=a[e].value;return b}function j(){var a=b(this),d;a.is(":password")||(a.data("password")?(d=a.next().show().focus(),b("label[for="+a.attr("id")+"]").attr("for",d.attr("id")),a.remove()):a.realVal()==a.attr("placeholder")&&(a.val(""),a.removeClass("placeholder")))}function k(){var a=b(this),d,c;placeholder=a.attr("placeholder");
b.trim(a.val()).length>0||(a.is(":password")?(c=a.attr("id")+"-clone",d=b("<input/>").attr(b.extend(m(this),{type:"text",value:placeholder,"data-password":1,id:c})).addClass("placeholder"),a.before(d).hide(),b("label[for="+a.attr("id")+"]").attr("for",c)):(a.val(placeholder),a.addClass("placeholder")))}var g="placeholder"in f.createElement("input"),h="placeholder"in f.createElement("textarea"),c=":input[placeholder]";b.placeholder={input:g,textarea:h};!i&&g&&h?b.fn.placeholder=function(){}:(!i&&g&&
!h&&(c="textarea[placeholder]"),b.fn.realVal=b.fn.val,b.fn.val=function(){var a=b(this),d;if(arguments.length>0)return a.realVal.apply(this,arguments);d=a.realVal();a=a.attr("placeholder");return d==a?"":d},b.fn.placeholder=function(){this.filter(c).each(k);return this},b(function(a){var b=a(f);b.on("submit","form",l);b.on("focus",c,j);b.on("blur",c,k);a(c).placeholder()}))})(jQuery,document,window.debug);